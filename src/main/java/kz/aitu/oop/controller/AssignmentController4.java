package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.repository.StudentFileRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

//import kz.aitu.oop.repository.StudentDBRepository;

@RestController
@RequestMapping("/api/task/4")
@AllArgsConstructor
public class AssignmentController4 {


    /**
     *
     * @param group
     * @return all student name by group name
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}")
    public ResponseEntity<?> getStudentsByGroup(@PathVariable("group") String group) throws FileNotFoundException {


        StudentFileRepository studentFileRepository = new StudentFileRepository();

        List<Student> studentList = studentFileRepository.getStudentsByGroup(group);

        String names = "";
        for (Student student: studentList) {
            names += student.getName() + "</br>";
        }

        return ResponseEntity.ok(names);
        //return ResponseEntity.ok(studentList);
    }

    /**
     *
     * @param group
     * @return stats by point letter (counting points): example  A-3, B-4, C-1, D-1, F-0
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}/stats")
    public ResponseEntity<?> getGroupStats(@PathVariable("group") String group) throws FileNotFoundException {

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        List<Student> studentList = studentFileRepository.getStudentsByGroup(group);

        Hashtable<String, Integer> pointLetter = new Hashtable<>();
        pointLetter.put("A", 0);
        pointLetter.put("B", 0);
        pointLetter.put("C", 0);
        pointLetter.put("D", 0);
        pointLetter.put("F", 0);

        for (Student student: studentList) {
            if(student.getPoint() >= 90) pointLetter.put("A", pointLetter.get("A") + 1 );
            else if(student.getPoint() >= 70) pointLetter.put("B", pointLetter.get("B") + 1 );
            else if(student.getPoint() >= 60) pointLetter.put("C", pointLetter.get("C") + 1 );
            else if(student.getPoint() >= 50) pointLetter.put("D", pointLetter.get("D") + 1 );
            else pointLetter.put("F", pointLetter.get("F") + 1 );
        }


        return ResponseEntity.ok(pointLetter);
    }

    /**
     *
     * @return top 5 students name by point
     * @throws FileNotFoundException
     */
    @GetMapping("/students/top")
    public ResponseEntity<?> getTopStudents() throws FileNotFoundException {

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        List<Student> studentList = studentFileRepository.getStudents();

        studentList.sort((o1, o2) -> {
            if(o1.getPoint() > o2.getPoint()) return -1;
            if(o1.getPoint() < o2.getPoint()) return 1;
            return 0;
        });

        return ResponseEntity.ok(studentList.subList(0, 5));
    }
}
