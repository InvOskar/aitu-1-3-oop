package kz.aitu.oop.examples.Quiz6;

import java.util.Scanner;

class Singleton {
    public  String str;
    public static Singleton instance = null;

    private Singleton(){

    }

    public void toClass(String str){
        this.str=str;
    }

    public static Singleton getSingleInstance(){
        if(instance == null) instance= new Singleton();
        return instance;
    }
    public String returnMassage(){
        return "Hello I am a singleton! Let me say " + str + " to you";
    }
}
