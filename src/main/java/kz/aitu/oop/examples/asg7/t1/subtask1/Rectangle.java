package kz.aitu.oop.examples.asg7.t1.subtask1;

public class Rectangle extends Shape {
    protected double width = 1;
    protected double length = 1;

    //constructor
    public Rectangle() {
    }
    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }
    public Rectangle(double width,double length, String color, boolean filled){
        this.width = width;
        this.length = length;
        color = super.color;
        filled = super.filled;
    }

    //getter & setter
    public double getWidth() {
        return width;
    }
    public void setWidth(double width) {
        this.width = width;
    }
    public double getLength() {
        return length;
    }
    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public double getArea() {
        return width*length;
    }

    @Override
    public double getPerimeter() {
        return 2*(width+length);
    }

    public String toString(){return color+","+filled+","+width+","+length;}

}
