package kz.aitu.oop.examples.asg7.t1.subtask1;

public class Circle extends Shape {
    protected double radius = 1;

    //constructor
    public Circle(double radius) {
        this.radius = radius;
    }
    public Circle() {
    }
    public Circle(double radius, String color, boolean filled){
        this.radius = radius;
        color = super.color;
        filled = super.filled;
    }


    //getter & setter
    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }


    @Override
    public double getArea() {
        return 3.14*radius*radius;
    }

    @Override
    public double getPerimeter() {
        return 3.14*(2*radius);
    }

    public String toString(){return color+","+filled+","+radius;}

}
