package kz.aitu.oop.examples.asg7.subtask3;

public interface Resizable {
    public double resize(int percent);
}
