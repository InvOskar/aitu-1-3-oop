package kz.aitu.oop.examples.asg7.subtask3;

public interface GeometricObject {
    public double getPerimeter();
    public double getArea();
}
