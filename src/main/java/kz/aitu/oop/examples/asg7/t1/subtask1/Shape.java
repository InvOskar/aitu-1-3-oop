package kz.aitu.oop.examples.asg7.t1.subtask1;

public abstract class Shape {
    protected String color = "red";
    protected boolean filled = true;

    //constructor
    public Shape() {}
    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    //getter
    public String getColor() {
        return color;
    }
    public boolean isFilled() {
        return filled;
    }
    //setter
    public void setColor(String color) {
        this.color = color;
    }
    public void setFilled(boolean filled) {
        this.filled = filled;
    }


    public abstract double getArea();
    public abstract double getPerimeter();
    public String toString(){return color+","+filled;}

}
