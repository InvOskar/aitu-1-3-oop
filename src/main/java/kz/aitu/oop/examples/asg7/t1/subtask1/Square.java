package kz.aitu.oop.examples.asg7.t1.subtask1;

public class Square extends Rectangle {

    //constructor
    public Square() {
    }
    public Square(double side) {
        length = side;
        width = side;
    }
    public Square(double side, String color, boolean filled) {
        length = side;
        width = side;
        color = super.color;
        filled = super.filled;
    }

    //getter & setter
    public double getSide() {
        return width;
    }
    public void setSide(double side) {
        length = side;
        width = side;
    }

    @Override
    public void setWidth(double side) {
        super.setWidth(width);
    }

    @Override
    public void setLength(double side) {
        super.setLength(length);
    }

    public String toString(){return color+","+filled+","+width+","+length;}
}
