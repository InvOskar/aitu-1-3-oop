package kz.aitu.oop.examples.asg7.subtask2;

public class Main {
    public static void main(String[] args){
        Movable c1 = new MovableCircle(5,6,1,1,5);
        System.out.println(c1);
        c1.moveUp();
        c1.moveLeft();
        System.out.println(c1);
    }
}
