package kz.aitu.oop.examples.asg7.subtask3;

import lombok.Data;

@Data
public class Circle implements GeometricObject{
    private double radius = 1;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double getPerimeter() {
        return 3.14*(2*radius);
    }

    @Override
    public double getArea() {
        return 3.14*radius*radius;
    }

    @Override
    public String toString() {
        return "radius: "+radius;
    }
}
