package kz.aitu.oop.examples.asg7.subtask2;

public interface Movable {
    public abstract void moveUp();
    public abstract void moveDown();
    public abstract void moveRight();
    public abstract void moveLeft();
}
