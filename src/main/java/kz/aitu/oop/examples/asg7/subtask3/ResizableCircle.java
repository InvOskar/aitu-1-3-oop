package kz.aitu.oop.examples.asg7.subtask3;

import lombok.Data;

@Data
public class ResizableCircle extends Circle implements Resizable{

    public ResizableCircle(double radius) {
        super(radius);
    }

    @Override
    public double resize(int percent) {
        double radius= getRadius();
        return radius*percent/100;
    }

    public String toString(){

        return "radius: "+ getRadius() ;
    }
}
