package kz.aitu.oop.examples.asg7.subtask2;
import lombok.Data;

@Data

public class MovableCircle implements Movable{
    private int radius;
    private MovablePoint center;


    public MovableCircle(int x, int y, int xSpeed, int ySpeed, int radius){
        this.radius = radius;
        this.center = new MovablePoint(x ,y ,xSpeed ,ySpeed);
    }

    @Override
    public void moveUp() {
        center.moveUp();
    }

    @Override
    public void moveDown() {
        center.moveDown();
    }

    @Override
    public void moveRight() {
        center.moveRight();
    }

    @Override
    public void moveLeft() {
        center.moveLeft();
    }

    public String toString(){
        return "radius: " + radius + " center: " + center;
    }
}
