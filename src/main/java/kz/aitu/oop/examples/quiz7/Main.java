package kz.aitu.oop.examples.quiz7;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Factory f1 = new Factory();
        Scanner input = new Scanner(System.in);
        String order =input.nextLine();
        Food f = f1.getFood(order);
        f.getType();
    }
}
