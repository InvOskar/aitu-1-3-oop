package kz.aitu.oop.examples.quiz7;

public class Factory {

    public Food getFood(String str){
        if(str.equals("Cake")){
            return new Dessert();
        }
        if(str.equals("Pizza")){
            return new FastFood();
        }
        return null;
    }
}
