package kz.aitu.oop.examples.polymorphism1914;

public class Bird extends Animal {

    public Bird(String name) {
        super(name);
    }

    public void fly() {
        System.out.println("Bird " + this.getName() + " flying...");
    }

    public void fly(int height) {
        System.out.println("Bird is flying " + height + " meter high");
    }
}
